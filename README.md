# IDATx2001 Programmering 2 - Oblig 3

_I denne innleveringen skal du jobbe med GUI, Lambdauttrykk og streams. I tillegg skal du (som alltid) lage enhetstester, opprette Maven-prosjekt, og praktisere versjonskontroll med Git både lokalt og remote mot GitLab._

### Problemstilling

Du skal utvikle et enkelt kortspill. Kortspillet består av en kortstokk som inneholder 52 kort. Det skal være mulig å dele ut en "hånd med kort" til en spiller. Det skal deles ut minimum 5 kort.

I kortspillet poker (blant annet), er en av kortkombinasjonene som gir poeng en [flush](https://en.wikipedia.org/wiki/Flush_(cards)). Ditt program skal blant annet sjekke korthånden for 5-korts flush (altså 5 kort av samme farge, som f.eks. 5 hjerter eller 5 kløver, eller 5 ruter osv), fortrinnsvis ved bruk av streams (med tilhørende funksjoner som filter, map osv.).

Du står helt fritt når det gjelder utforming av GUI. I enkleste form kan du presentere en korthånd som en streng på formen "H4 H12 C3 D11 S1", der bokstavene 'H', 'D', 'C' og 'S' står for henholdsvis "Hearts", "Diamonds", "Clubs" og "Spades".

### Screenshots

![Screenshot](/images/screenshot.png)
<br>

![Screenshot](/images/screenshot2.png)
