module no.ntnu.idatt {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens ntnu.idat.oblig3.controller to javafx.fxml;
    exports ntnu.idat.oblig3;
}