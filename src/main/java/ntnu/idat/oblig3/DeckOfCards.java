package ntnu.idat.oblig3;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    private ArrayList<PlayingCard> deck;

    private final char[] suit = {'S', 'H', 'D', 'C'};
    private final int[] face = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    
    /**
     * Creates an instance of a {@code DeckOfCards} with all 52 possible playing cards.
     */
    public DeckOfCards() {
        this.deck = new ArrayList<>();

        for (int f : face) {
            for (char c : suit) {
                this.deck.add(new PlayingCard(c, f));
            }
        }
    }

    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

    /**
     * Deals a hand with a given amount of cards
     * @param n amount of cards
     * @return List of {@code PlayingCard}
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        Random random = new Random();
        ArrayList<PlayingCard> hand = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            PlayingCard card = this.deck.get(random.nextInt(52));
            if (!hand.contains(card)) {
                hand.add(card);
            } else {i--;}
        }
        return hand;
    }
}
