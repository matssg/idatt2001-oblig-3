package ntnu.idat.oblig3.controller;

import javafx.fxml.FXML;

import ntnu.idat.oblig3.*;

import javafx.stage.Stage;
import javafx.event.ActionEvent;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class MainController {
    @FXML public AnchorPane ap;
    @FXML public AnchorPane topBar;
    @FXML public Button closeButton;
    @FXML public Button minimizeButton;
    @FXML public Button dealButton;
    @FXML public Button checkButton;

    @FXML public ImageView card1;
    @FXML public ImageView card2;
    @FXML public ImageView card3;
    @FXML public ImageView card4;
    @FXML public ImageView card5;

    @FXML public Label labelQS;
    @FXML public Label labelCardH;
    @FXML public Label labelSum;
    @FXML public Label labelFlush;

    private DeckOfCards deck = new DeckOfCards();
    private HandOfCards hand;

    public void initialize() {
        // Code for dragging undecorated window by topBar
        topBar.setOnMousePressed(pressEvent -> {
            topBar.setOnMouseDragged(dragEvent -> {
                App.primaryStage.setX(dragEvent.getScreenX() - pressEvent.getSceneX());
                App.primaryStage.setY(dragEvent.getScreenY() - pressEvent.getSceneY());
            });
        });
    }

    @FXML
    public void handleCloseButtonAction(ActionEvent event) {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    public void handleMinimizeButtonAction(ActionEvent event) {
        Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    public void handleDealButtonAction(ActionEvent event) {
        // Clear information on screen
        labelQS.setText("");
        labelCardH.setText("");
        labelSum.setText("");
        labelFlush.setText("");

        // Deal new hand with 5 cards
        // Possible to deal more then 5 cards but maximum 5 cards will be displayed
        hand = new HandOfCards(deck.dealHand(5));

        // Display cards
        if (hand.getHand().size() >= 1) {card1.setImage(new Image(getClass().getResourceAsStream("/images/cards/card" + hand.getHand().get(0).toString() + ".png")));}
        if (hand.getHand().size() >= 2) {card2.setImage(new Image(getClass().getResourceAsStream("/images/cards/card" + hand.getHand().get(1).toString() + ".png")));}
        if (hand.getHand().size() >= 3) {card3.setImage(new Image(getClass().getResourceAsStream("/images/cards/card" + hand.getHand().get(2).toString() + ".png")));}
        if (hand.getHand().size() >= 4) {card4.setImage(new Image(getClass().getResourceAsStream("/images/cards/card" + hand.getHand().get(3).toString() + ".png")));}
        if (hand.getHand().size() >= 5) {card5.setImage(new Image(getClass().getResourceAsStream("/images/cards/card" + hand.getHand().get(4).toString() + ".png")));}
    }

    @FXML
    public void handleCheckButtonAction(ActionEvent event) {
        if (hand != null) {
            // Check for flush
            if (hand.checkForFlush()) {labelFlush.setText("Yes");} 
            else {labelFlush.setText("No");}

            // Sum of cards
            labelSum.setText(String.valueOf(hand.sumOfHand()));

            // Check for QueenOfSpades
            if (hand.checkForQueenOfSpades()) {labelQS.setText("Yes");}
            else {labelQS.setText("No");}
            
            //Display cards of heart
            labelCardH.setText(hand.allCardsOfHeart());
        }
    }
}

