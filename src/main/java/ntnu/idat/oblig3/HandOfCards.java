package ntnu.idat.oblig3;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class HandOfCards {
    private ArrayList<PlayingCard> hand;
    
    /**
     * Creates an instance of a {@code HandOfCards} using already dealt list of cards.
     */
    public HandOfCards(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    public ArrayList<PlayingCard> getHand() {
        return hand;
    }

    /**
     * Checks if the hand contains 5 cards or more of the same suit
     * @param hand
     * @return {@code True} if it does contain the cards
     *         {@code False} if it does not contain the cards
     */
    public boolean checkForFlush() {
        if (getHand().stream().filter(p -> p.getSuit() == 'S').count() >= 5 ||
            getHand().stream().filter(p -> p.getSuit() == 'H').count() >= 5 ||
            getHand().stream().filter(p -> p.getSuit() == 'D').count() >= 5 ||
            getHand().stream().filter(p -> p.getSuit() == 'C').count() >= 5) {
                return true;
        } else {
            return false;
        }
    } 

    /**
     * Adds together all card values
     * @return the sum of the hand
     */
    public int sumOfHand() {
        return (getHand().stream().map(PlayingCard::getFace).reduce((a,b) -> a+b).get());
    }

    /**
     * List all cards of type 'Heart' in hand
     * @return {@code String} of cards of type 'Heart'
     */
    public String allCardsOfHeart() {
        return getHand().stream().filter(p -> p.getSuit() == 'H').map(PlayingCard::toString).collect(Collectors.joining(" "));
    }

    /**
     * Checks the hand for the card 'Queen of Spades'
     * @return {@code True} if Queen of Spades is present
     *         {@code False} if not
     */
    public boolean checkForQueenOfSpades() {
        return (getHand().stream().anyMatch(p -> p.toString().equals("S12")));
    }
}
