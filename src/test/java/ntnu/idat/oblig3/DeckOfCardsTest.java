package ntnu.idat.oblig3;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class DeckOfCardsTest {
    private final DeckOfCards deck = new DeckOfCards();
    
    @Test
    void deck_contains_exactly_52_cards() {
        Assertions.assertTrue(deck.getDeck().size() == 52);
    }

    @Test
    void dealHand_deals_the_correct_amount_of_cards() {
        Assertions.assertTrue(deck.dealHand(10).size() == 10);
    }
}
