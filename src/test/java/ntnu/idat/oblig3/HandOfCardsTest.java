package ntnu.idat.oblig3;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;

public class HandOfCardsTest {
    private HandOfCards hand;

    ArrayList<PlayingCard> allDiamonds = new ArrayList<>();
    ArrayList<PlayingCard> mixed = new ArrayList<>();

    void setUp() {
        allDiamonds.add(new PlayingCard('D', 2));
        allDiamonds.add(new PlayingCard('D', 11));
        allDiamonds.add(new PlayingCard('D', 1));
        allDiamonds.add(new PlayingCard('D', 5));
        allDiamonds.add(new PlayingCard('D', 4));

        mixed.add(new PlayingCard('C', 2));
        mixed.add(new PlayingCard('D', 11));
        mixed.add(new PlayingCard('S', 12));
        mixed.add(new PlayingCard('H', 5));
        mixed.add(new PlayingCard('H', 4));
    }

    @Nested
    class _flush {

        @Test
        void holding_a_flush() {
            setUp();
            hand = new HandOfCards(allDiamonds);

            Assertions.assertTrue(hand.checkForFlush());
        }

        @Test
        void not_holding_a_flush() {
            setUp();
            hand = new HandOfCards(mixed);

            Assertions.assertFalse(hand.checkForFlush());
        }
    }

    @Test
    void sum_of_hand() {
        setUp();
        hand = new HandOfCards(mixed);
            
        Assertions.assertEquals(34, hand.sumOfHand());
    }

    @Test
    void all_cards_of_heart() {
        setUp();
        hand = new HandOfCards(mixed);
            
        Assertions.assertEquals("H5 H4", hand.allCardsOfHeart());
    }

    @Nested
    class _queen_of_spades {

        @Test
        void queen_of_spades_exists() {
            setUp();
            hand = new HandOfCards(mixed);
            
            Assertions.assertTrue(hand.checkForQueenOfSpades());
        }

        @Test
        void queen_of_spades_does_not_exist() {
            setUp();
            hand = new HandOfCards(allDiamonds);
            
            Assertions.assertFalse(hand.checkForQueenOfSpades());
        }
    } 
}
